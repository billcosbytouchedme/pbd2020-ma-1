package si.uni_lj.fri.pbd.miniapp1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.fragment_message, container, false);
        SharedPreferences sp=view.getContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        //fetch whole SP file
        Map<String,?> all=sp.getAll();
        final List<String> phones=new ArrayList<>();
        final List<String> emails=new ArrayList<>();
        //find the entries that hold contact data and store it
        for(Map.Entry<String,?> pair:all.entrySet()){
            if(pair.getKey().contains("contact:")){
                String phone=((String)pair.getValue()).split("&")[1];
                String email=((String)pair.getValue()).split("&")[2];
                if(!phone.equals("No phone number available")){
                    phones.add(phone);
                }
                if(!email.equals("No email available")){
                    emails.add(email);
                }
            }
        }
        //send mms
        view.findViewById(R.id.msgbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!phones.isEmpty()){
                    StringBuilder uri=new StringBuilder();
                    for(int i=0;i<phones.size();i++){
                        uri.append(phones.get(i));
                        if(i<phones.size()){
                            uri.append(";");
                        }
                    }
                    Intent intent = new Intent(Intent.ACTION_SENDTO,Uri.parse("mmsto:"+uri.toString()));
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    } else{
                        Snackbar.make(view,"Something went wrong trying to compose the MMS :'(", Snackbar.LENGTH_LONG).show();
                    }
                } else{
                    Snackbar.make(view,"You did not select any phone number :'(", Snackbar.LENGTH_LONG).show();
                }
            }
        });
        //send mail
        view.findViewById(R.id.mailbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!emails.isEmpty()){
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:"));
                    String[] mails = new String[emails.size()];
                    for(int i=0;i<emails.size();i++){
                        mails[i]=emails.get(i);
                    }
                    intent.putExtra(Intent.EXTRA_EMAIL,mails);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    } else{
                        Snackbar.make(view,"Something went wrong trying to compose the email :'(", Snackbar.LENGTH_LONG).show();
                    }
                } else{
                    Snackbar.make(view,"You did not select any emails :'(", Snackbar.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }
}