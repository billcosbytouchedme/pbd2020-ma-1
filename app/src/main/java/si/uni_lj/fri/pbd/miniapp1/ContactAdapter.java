package si.uni_lj.fri.pbd.miniapp1;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder>{

    List<ContactModel> contacts;

    public static class ContactViewHolder extends RecyclerView.ViewHolder{
        public TextView name;
        public TextView email;
        public TextView phone;
        public CheckBox checkbox;
        public ContactViewHolder(View view) {
            super(view);
            name=view.findViewById(R.id.contact_name);
            phone=view.findViewById(R.id.contact_phone);
            email=view.findViewById(R.id.contact_email);
            checkbox=view.findViewById(R.id.contact_checkbox);
        }
    }

    //the contacts which were fetched in fragment class
    public ContactAdapter(List<ContactModel> c){
        contacts=c;
    }

    //inflates an individual list entry
    @NonNull
    @Override
    public ContactAdapter.ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.contact,parent,false));
    }

    //connects the list entry UI elements with data from contacts
    @Override
    public void onBindViewHolder(@NonNull ContactAdapter.ContactViewHolder holder, int position) {
        final ContactModel contact=contacts.get(position);
        holder.name.setText(contact.getName());
        holder.phone.setText(contact.getPhoneNumber());
        holder.email.setText(contact.getEmail());
        //if there is an entry in SP, then user checked this list item before, also update contact data
        SharedPreferences sp=holder.itemView.getContext().getSharedPreferences("preferences",Context.MODE_PRIVATE);
        String storedinfo=sp.getString("contact:"+contact.getName(),null);
        if(storedinfo!=null) {
            holder.checkbox.setChecked(true);
            SharedPreferences.Editor eddie=sp.edit();
            eddie.putString("contact:"+contact.getName(),contact.toString());
            eddie.apply();
        }
        //memorizing user choices
        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox c=(CheckBox)v;
                SharedPreferences sp=c.getContext().getSharedPreferences("preferences",Context.MODE_PRIVATE);
                SharedPreferences.Editor eddie=sp.edit();
                if(c.isChecked()){
                    eddie.putString("contact:"+contact.getName(),contact.toString());
                } else {
                    eddie.remove("contact:"+contact.getName());
                }
                eddie.apply();
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }
}
