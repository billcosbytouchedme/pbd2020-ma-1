package si.uni_lj.fri.pbd.miniapp1;

import androidx.annotation.NonNull;

public class ContactModel {
    private String name;
    private String email;
    private String phoneNumber;

    public ContactModel(String n,String e,String p){
        this.email=e;
        this.name=n;
        this.phoneNumber=p;
    }

    @NonNull
    @Override
    public String toString(){
        return name+"&"+phoneNumber+"&"+email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
