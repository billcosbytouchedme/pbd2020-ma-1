package si.uni_lj.fri.pbd.miniapp1;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class ContactsFragment extends Fragment {

    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;
    private List<ContactModel> contacts=new ArrayList<ContactModel>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
            readcontacts();
        }
        View view=inflater.inflate(R.layout.fragment_contacts, container, false);
        RecyclerView rview=view.findViewById(R.id.contact_list);
        layoutManager = new LinearLayoutManager(container.getContext());
        rview.setLayoutManager(layoutManager);
        adapter=new ContactAdapter(contacts);
        rview.setAdapter(adapter);
        return view;
    }

    private void readcontacts(){
        ContentResolver contentResolver=getContext().getContentResolver();
        //queries every contact
        Cursor cursor=contentResolver.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
        assert cursor != null;
        if (cursor.getCount() > 0) {
            //goes over contacts
            while (cursor.moveToNext()) {
                //fetches id and name of current contact
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String phone="No phone number available";
                String email="No email available";
                //checks if phone number exists, queries and retrieves it
                // only need 1 number
                if(cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))>0){
                    Cursor phonecursor=contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{id},null);
                    //checks if any exist
                    assert phonecursor != null;
                    if(phonecursor.moveToNext()){
                        phone=phonecursor.getString(phonecursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phonecursor.close();
                    }
                }

                //queries and retrieves email
                //only need 1 email
                Cursor emailcursor=contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                //checks if any exist
                assert emailcursor != null;
                if(emailcursor.moveToNext()){
                    email=emailcursor.getString(emailcursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                }
                emailcursor.close();

                contacts.add(new ContactModel(name,email,phone));
            }
        }
        cursor.close();
    }
}
